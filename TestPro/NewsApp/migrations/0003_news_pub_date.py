# Generated by Django 3.1.3 on 2020-11-30 06:08

import datetime
from django.db import migrations, models
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('NewsApp', '0002_sportnews'),
    ]

    operations = [
        migrations.AddField(
            model_name='news',
            name='pub_date',
            field=models.DateField(default=datetime.datetime(2020, 11, 30, 6, 8, 58, 266745, tzinfo=utc)),
        ),
    ]
