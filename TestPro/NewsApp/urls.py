
from django.urls import path
from .views import Newsp, Home, contact, NewsDate, register, addUser, modelform, addModalForm

urlpatterns = [
    path('', Home, name = 'home'),
    path('news/', Newsp, name = 'news'),
    path('newsdate/<str:year>', NewsDate, name='newsdate'),
    path('contact/', contact, name = 'contact'),
    path('signup/', register, name = 'register'),
    path('addUser/', addUser, name = 'addUser'),
    path('modalform/', modelform, name = 'form'),
    path('addmodalform/', addModalForm, name = 'modelform')
]
