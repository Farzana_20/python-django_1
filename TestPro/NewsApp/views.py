from django.shortcuts import render, redirect
from django.shortcuts import HttpResponse
from .models import News
from .forms import RegistrationForm, RegistrationModal
from .models import RegistrationData
from django.contrib import messages

# Create your views here.

def Home(request):
   # return HttpResponse("This is our homepage")
   context= {
      "name":"Rumpa"
   }
   
   return render(request, 'home.html' , context)

def Newsp(request):  #need to change function name news to newsP because here from model we import news which is class and creates conflict
    #return HttpResponse("This is our latest news")
    obj = News.objects.get(id=1)

    context = {
       #"list":["Python", "Java", "C++", "C#"],
       #"mynum":50

       "data":obj
    }
    return render(request, 'news.html' , context)

def NewsDate(request, year):

   article_list = News.objects.filter(pub_date = year)

   context = {
      'year':year,
      'article_list':article_list
   }

   return render (request, 'newsdate.html', context)

def contact(request):
   # return HttpResponse("This is our contact")
    return render(request, 'contact.html') 

def register(request):
    
    context = {
       "form":RegistrationForm
    }
    return render(request, 'signup.html',context)

def addUser(request):

   form = RegistrationForm(request.POST)

   if form.is_valid():
       myregister = RegistrationData(username = form.cleaned_data['username'],
                                   password=form.cleaned_data['password'],
                                   email=form.cleaned_data['email'],
                                   phone=form.cleaned_data['phone'])

       myregister.save()
       messages.add_message(request, messages.SUCCESS, "You have signup successfully")
   
  
   return redirect ('register')


def modelform(request):

   context = {
       "modalform":RegistrationModal
    }
   return render(request, 'modalform.html', context)

def addModalForm(request):

   mymodalform = RegistrationModal(request.POST)

   if mymodalform.is_valid():
      mymodalform.save()

   return redirect('form')


 